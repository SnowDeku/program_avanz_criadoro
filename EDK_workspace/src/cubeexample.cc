#include <ESAT/window.h>
#include <ESAT/input.h>
#include <ESAT/draw.h>
#include <ESAT/time.h>

#include <EDK3/geometry.h>
#include <EDK3/camera.h>
#include <EDK3/drawable.h>
#include <EDK3/matdiffuse.h>
#include <EDK3/texture.h>
#include <EDK3/dev/gpumanager.h>

#include <iostream>
#include <cmath>
#include "math.h"
#include <time.h>
#include <stdio.h>

double previus_time;
int const arry_cube_size = 55;
struct GameState {
	EDK3::ref_ptr<EDK3::Camera> camera;
	EDK3::ref_ptr<EDK3::Node> root;
};

//-----------------------------------------------------------------------------------------------------

void prepare(GameState *state) {
	//Funcion de OpenGL comprueba si hay errores, 0 si no hay
	EDK3::dev::GPUManager::CheckGLError("Prepare Start");
	
    // Material
	EDK3::ref_ptr<EDK3::MatDiffuse> diffuse_material;
    EDK3::ref_ptr<EDK3::MatDiffuse::Settings> diffuse_material_settings_cubes[arry_cube_size][arry_cube_size];

	// Create a Drawable Node (Geometry+Material+Settings)
	EDK3::ref_ptr<EDK3::Drawable> drawable_cubes[arry_cube_size][arry_cube_size];

	//Geometry
	EDK3::ref_ptr<EDK3::Geometry> array_cubes[arry_cube_size][arry_cube_size];

	//Material Alloc
	diffuse_material.alloc();

	// Nodes
	EDK3::Node *root = state->root.alloc();

	for (int i = 0; i < arry_cube_size; i++) {
		for (int j = 0; j < arry_cube_size; j++) {
			//Create
			EDK3::CreateCube(&array_cubes[i][j], 1.0f, true, false);

			//Alloc
			diffuse_material_settings_cubes[i][j].alloc();

			//Graphic
			drawable_cubes[i][j].alloc();
			drawable_cubes[i][j]->set_geometry(array_cubes[i][j].get());
			drawable_cubes[i][j]->set_material(diffuse_material.get());
			drawable_cubes[i][j]->set_material_settings(diffuse_material_settings_cubes[i][j].get());

			// Transform
			drawable_cubes[i][j]->set_position((float)i-27, (float)j-27, 35.0f);
			drawable_cubes[i][j]->set_scale(0.5f, 0.5f, 0.5f);
			drawable_cubes[i][j]->set_HPR(360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX);

			//Add to root
			root->addChild(drawable_cubes[i][j].get());

			//Color
			float cube_color[] = { rand()%255 / 255.0f,  rand() % 255 / 255.0f,  rand() % 255 / 255.0f, 1.0f };
			diffuse_material_settings_cubes[i][j]->set_color(cube_color);
		}
	}

	//-----------------------------------------------------------------------------------------------------

	// Create a Camera
	state->camera.alloc();
	float pos[] = { 120,140,120 };
	float view[] = { -120,-140,-120 };
	state->camera->set_position(pos);
	state->camera->set_view_direction(view);
	state->camera->setupPerspective(70, 1280.0f / 720.0f, 1.0f, 1500.0f);
	state->camera->set_clear_color(0.99f, 0.99f, 0.99f, 1.0f);
	EDK3::dev::GPUManager::CheckGLError("Prepare END");
}


void render_function(GameState *state) {
	
	// Update
	//
	// -- Orbital camera: this will make move the camera to the rigth side
	// state->root->set_rotation_y(5.0f * ESAT::Time() / 1000.0f);

	// For Every frame... determine what's visible:
	// El doCullse usa antes del cliping para ver las figuras 
	// que van estar dentro de la camara y luego es el clipping
	// el que corta la figura para no  pintarla entera.
	state->camera->doCull(state->root.get());
	
	// Render
	EDK3::dev::GPUManager::CheckGLError("begin Render-->");
	state->camera->doRender();
	EDK3::dev::GPUManager::CheckGLError("end Render-->");


	double mx = ESAT::MousePositionX();
	double my = ESAT::MousePositionY();
	double p = sin(-my / 200) * 220;
	float zoom_factor = 0.014f;
	float pos[] = {
		(float)(p*cos(mx / 100)) * zoom_factor,
		(float)(cos(-my / 200) * 220) * zoom_factor,
		(float)(p*sin(mx / 100)) * zoom_factor };
	//If we change the view  we change the way we move the camera for example change the Y  to + or - will invert the Y move
	float view[] = { -pos[0],pos[1],-pos[2] };
	state->camera->set_position(pos);
	state->camera->set_view_direction(view);
}

int ESAT::main(int argc, char **argv) {
	
	srand(time(NULL));

	// State
	GameState state;
	ESAT::WindowInit(1280, 720);
	ESAT::DrawSetTextFont("test.ttf");
	ESAT::DrawSetTextSize(18);
	ESAT::DrawSetFillColor(253, 255, 255, 128);
	prepare(&state);
	

	// Main loop
	while (ESAT::WindowIsOpened() && !ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {
		
		double currentTime = RawTime();
		double delta_time = (currentTime - previus_time) / 1000.0f;
		previus_time = currentTime;
		
		render_function(&state);
		ESAT::DrawBegin();
		ESAT::DrawText(10, 20, "equalizer");
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	return 0;
}
