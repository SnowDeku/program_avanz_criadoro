#include <ESAT/window.h>
#include <ESAT/input.h>
#include <ESAT/draw.h>
#include <ESAT/time.h>

#include <EDK3/geometry.h>
#include <EDK3/camera.h>
#include <EDK3/drawable.h>
#include <EDK3/matdiffuse.h>
#include <EDK3/texture.h>
#include <EDK3/dev/gpumanager.h>

#include <iostream>
#include <cmath>
#include "math.h"
#include <time.h>
#include <stdio.h>

double previus_time;
int const arry_cube_size = 7;
struct GameState {
	EDK3::ref_ptr<EDK3::Camera> camera;
	EDK3::ref_ptr<EDK3::Node> root;
};

//-----------------------------------------------------------------------------------------------------

void prepare(GameState *state) {
	//Funcion de OpenGL comprueba si hay errores, 0 si no hay
	EDK3::dev::GPUManager::CheckGLError("Prepare Start");
	
    // Material
	EDK3::ref_ptr<EDK3::MatDiffuse> diffuse_material;
    EDK3::ref_ptr<EDK3::MatDiffuse::Settings> diffuse_material_settings_cubes[arry_cube_size];

	// Create a Drawable Node (Geometry+Material+Settings)
	EDK3::ref_ptr<EDK3::Drawable> drawable_planets[arry_cube_size];
	// Create a Node for rotate the planets
	EDK3::ref_ptr<EDK3::Node> r_planets[arry_cube_size];

	//Geometry
	EDK3::ref_ptr<EDK3::Geometry> planets[arry_cube_size];
	

	//Material Alloc
	diffuse_material.alloc();

	// Nodes
	EDK3::Node *root = state->root.alloc();

	for (int i = 0; i < arry_cube_size-3; i++) {
	
			//Create the Planets
			EDK3::CreateCube(&planets[i], 1.0f, true, false);
			//Alloc
			diffuse_material_settings_cubes[i].alloc();
			//Graphic
			drawable_planets[i].alloc();
			drawable_planets[i]->set_geometry(planets[i].get());
			drawable_planets[i]->set_material(diffuse_material.get());
			drawable_planets[i]->set_material_settings(diffuse_material_settings_cubes[i].get());
			// Transform
			drawable_planets[i]->set_position((float)i*1.5f, (float)0.f,5.0f);
			drawable_planets[i]->set_scale(0.5f, 0.5f, 0.5f);
			drawable_planets[i]->set_HPR(360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX);
			//Color
			float cube_color[] = { rand()%255 / 255.0f,  rand() % 255 / 255.0f,  rand() % 255 / 255.0f, 1.0f };
			diffuse_material_settings_cubes[i]->set_color(cube_color);

			//Create the r_Planets

			diffuse_material_settings_cubes[i].alloc();
			//Graphic
			r_planets[i].alloc();
			// Transform
			r_planets[i]->set_position(0.f, 0.f, 5.0f);
	
			//Add first the r_planets then add the planet as a child
			root->addChild(r_planets[i].get());
			r_planets[i]->addChild(drawable_planets[i].get());
	}

	//-----------------------------------------------------------------------------------------------------
	//Add_to_root in form r_planet child planet
	//Only once
	root->addChild(drawable_planets[0].get());
	for (int i = 3; i < arry_cube_size; i++) {

		//Create the Planets
		EDK3::CreateCube(&planets[i], 1.0f, true, false);
		//Alloc
		diffuse_material_settings_cubes[i].alloc();
		//Graphic
		drawable_planets[i].alloc();
		drawable_planets[i]->set_geometry(planets[i].get());
		drawable_planets[i]->set_material(diffuse_material.get());
		drawable_planets[i]->set_material_settings(diffuse_material_settings_cubes[i].get());
		// Transform
		drawable_planets[i]->set_position((float)i*1.5f, (float)0.f, 5.0f);
		drawable_planets[i]->set_scale(0.5f, 0.5f, 0.5f);
		drawable_planets[i]->set_HPR(360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX);
		//Color
		float cube_color[] = { rand() % 255 / 255.0f,  rand() % 255 / 255.0f,  rand() % 255 / 255.0f, 1.0f };
		diffuse_material_settings_cubes[i]->set_color(cube_color);

		//Create the r_Planets

		diffuse_material_settings_cubes[i].alloc();
		//Graphic
		r_planets[i].alloc();
		// Transform
		r_planets[i]->set_position(root->child(i-3)->);

		//Add first the r_planets then add the planet as a child
		root->addChild(r_planets[i].get());
		r_planets[i]->addChild(drawable_planets[i].get());
	}


	// Create a Camera
	state->camera.alloc();
	float pos[] = { 1,1,1 };
	float view[] = { -120,-140,-120 };
	state->camera->set_position(pos);
	state->camera->set_view_direction(view);
	state->camera->setupPerspective(70, 1280.0f / 720.0f, 1.0f, 1500.0f);
	state->camera->set_clear_color(0.99f, 0.99f, 0.99f, 1.0f);
	EDK3::dev::GPUManager::CheckGLError("Prepare END");
}

void update(GameState *state) {

	//Rotate Sun
	state->root->child(4)->set_rotation_x(2.0f*ESAT::Time() / 100);

	//Rotate the r_planets
	state->root->child(1)->set_rotation_y(2.0f*ESAT::Time() / 100);
	state->root->child(2)->set_rotation_y(4.0f*ESAT::Time() / 100);
	state->root->child(3)->set_rotation_y(-2.0f*ESAT::Time() / 100);
	
	//Rotate the planets
	state->root->child(1)->child(0)->set_rotation_x(2.0f*ESAT::Time() / 100);
	state->root->child(2)->child(0)->set_rotation_x(4.0f*ESAT::Time() / 100);
	state->root->child(3)->child(0)->set_rotation_x(-2.0f*ESAT::Time() / 100);



	//Camera things

	double mx = ESAT::MousePositionX();
	double my = ESAT::MousePositionY();
	double p = sin(-my / 200) * 220;
	float zoom_factor = 0.014f;
	float pos[] = {
		(float)(p*cos(mx / 100)) * zoom_factor,
		(float)(cos(-my / 200) * 220) * zoom_factor,
		(float)(p*sin(mx / 100)) * zoom_factor };
	float view[] = { -pos[0],-pos[1],-pos[2] };
	state->camera->set_position(pos);
	state->camera->set_view_direction(view);
}

void render_function(GameState *state) {
	
	// Update
 //	state->root->set_rotation_y(5.0f * ESAT::Time() / 1000.0f);

	// For Every frame... determine what's visible:
	// El doCullse usa antes del cliping para ver las figuras 
	// que van estar dentro de la camara y luego es el clipping
	// el que corta la figura para no  pintarla entera.
	state->camera->doCull(state->root.get());
	
	// Render
	EDK3::dev::GPUManager::CheckGLError("begin Render-->");
	state->camera->doRender();
	EDK3::dev::GPUManager::CheckGLError("end Render-->");

	// -- Orbital camera:
	double mx = ESAT::MousePositionX();
	double my = ESAT::MousePositionY();
	double p = sin(-my / 200) * 220;
	float zoom_factor = 0.014f;
	float pos[] = {
		(float)(p*cos(mx / 100)) * zoom_factor,
		(float)(cos(-my / 200) * 220) * zoom_factor,
		(float)(p*sin(mx / 100)) * zoom_factor };
	float view[] = { -pos[0],-pos[1],-pos[2] };
	state->camera->set_position(pos);
	state->camera->set_view_direction(view);
}

int ESAT::main(int argc, char **argv) {
	
	srand(time(NULL));

	// State
	GameState state;
	ESAT::WindowInit(1280, 720);
	ESAT::DrawSetTextFont("test.ttf");
	ESAT::DrawSetTextSize(18);
	ESAT::DrawSetFillColor(253, 255, 255, 128);
	prepare(&state);
	

	// Main loop
	while (ESAT::WindowIsOpened() && !ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {
		
		double currentTime = RawTime();
		double delta_time = (currentTime - previus_time) / 1000.0f;
		previus_time = currentTime;


		//Update
		update(&state);


		//Render
		render_function(&state);


		ESAT::DrawBegin();
		ESAT::DrawText(10, 20, "equalizer");
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	return 0;
}
