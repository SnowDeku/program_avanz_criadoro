#pragma once

// ----------------------------------------------------------------------------
// Copyright (C) 2017 Lucas Gonzalez
// ----------------------------------------------------------------------------
//

#include <EDK3\texture.h>
#include <EDK3\dev\opengl.h>
#include <EDK3\ref_ptr.h>

namespace SNOW {

  class Texture : public virtual EDK3::Texture {
  public:
	 Texture();
	 virtual ~Texture();
	 void init(EDK3::Texture::Type t, Format internal_format, unsigned int width, unsigned int height = 1, unsigned int depth = 1);

    // Uploading Data to the texture ------------------

    virtual void set_data(
        const Format f, // DataFormat of *data
        const EDK3::Type t,   // DataType of each element of *data
        const void *data,   // the data itself
        unsigned int mipmap_LOD = 0); // The LOD to fill (mipmapping only)

    // use the texture in the given texture unit
    virtual void bind(unsigned int textUnit) const;
    virtual unsigned int internal_id() const;
	static bool Load(const char *filename, EDK3::ref_ptr<Texture> *output_tex);
    // Other functions to implement (all of them must be implemented)
    // Also, remember to call the parent function in order to set the value.
	virtual void set_min_filter(Filter f);
	virtual void set_mag_filter(Filter f);
	virtual void set_wrap_s(Wrap c);
	virtual void set_wrap_t(Wrap c);
	virtual void set_wrap_r(Wrap c);
	virtual void generateMipmaps() const;

    protected:

    Texture(const Texture&);
    Texture& operator=(const Texture&);

    private:

	GLenum target_;
	GLuint id_;



  };

} /* end of SNOW Namespace */

