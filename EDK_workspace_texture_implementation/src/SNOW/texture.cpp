/*
Jorge Criado Ros
2 Programacion 

ESAT
*/

#include "texture.h"

/*
* Convert Functions to OPENGL
*/
GLenum conver_texture_type(EDK3::Texture::Type  type_) {

	switch (type_)
	{
		case 0:  return GL_TEXTURE_1D;
		case 1:  return GL_TEXTURE_2D;
		case 2:  return GL_TEXTURE_3D;
		case 3:  return GL_TEXTURE_1D_ARRAY;
		case 4:  return GL_TEXTURE_2D_ARRAY;
		case 5:  return GL_TEXTURE_RECTANGLE;
		case 6:  return GL_TEXTURE_CUBE_MAP;
		case 7:  return GL_TEXTURE_CUBE_MAP_ARRAY;
		case 8:  return GL_TEXTURE_BUFFER;
		case 9:  return GL_TEXTURE_2D_MULTISAMPLE;
		case 10:  return GL_TEXTURE_2D_MULTISAMPLE_ARRAY;
	}
 }

GLenum conver_format(EDK3::Texture::Format format) {
	switch (format)
	{
	case 0: return GL_RED;
	case 1: return GL_RG;
	case 2: return GL_RGB;
	case 3: return GL_BGR;
	case 4: return GL_RGBA;
	case 5: return GL_BGRA;
	case 6: return GL_RED_INTEGER;
	case 7: return GL_RG_INTEGER;
	case 8: return GL_RGB_INTEGER;
	case 9: return GL_BGR_INTEGER;
	case 10: return GL_RGBA_INTEGER;
	case 11: return GL_BGRA_INTEGER;
	case 12: return GL_STENCIL_INDEX;
	case 13: return GL_DEPTH_COMPONENT;
	case 14: return GL_DEPTH_STENCIL;
	}
}

GLenum conver_data_type(EDK3::Type t) {
	switch (t)
	{
	case 0: return GL_UNSIGNED_BYTE;
	case 1: return GL_BYTE, GL_UNSIGNED_SHORT;
	case 2: return GL_SHORT, GL_UNSIGNED_INT;
	case 3: return GL_INT, GL_FLOAT;
	}
}

GLenum conver_wrap(EDK3::Texture::Wrap wrap) {

	switch (wrap)
	{
		case 0: return GL_TEXTURE_WRAP_S;
		case 1: return GL_TEXTURE_WRAP_T;
		case 2: return GL_TEXTURE_WRAP_R;
	}
}

/*
* Creator & Destructor
*/
SNOW::Texture::Texture() {
	glGenTextures(1,&(SNOW::Texture::id_));
}

SNOW::Texture::~Texture() {

	glDeleteTextures(1,&(SNOW::Texture::id_));
}

/*
* All other functions implemented
*/
void SNOW::Texture::init(EDK3::Texture::Type t, Format internal_format, unsigned int width, unsigned int height, unsigned int depth) {

	init(t, internal_format, width, height, depth);
}

bool SNOW::Texture::Load(const char *filename, EDK3::ref_ptr<Texture> *output_text) {
	//Load all the data
	unsigned char* pixel_data = loadTextureData(data, &width_, &height_);
}

void SNOW::Texture::set_data(const Format f,const EDK3::Type t,const void *data,unsigned int mipmap_LOD ) {

	glTexImage2D(conver_texture_type(EDK3::Texture::type()),
				mipmap_LOD,
				conver_format(EDK3::Texture::format()),
				EDK3::Texture::width(), 
				EDK3::Texture::height(),0,
				conver_format(f),
				conver_data_type(t), data);

}

void SNOW::Texture::bind(unsigned int textUnit) const {

	glBindTexture(conver_texture_type(EDK3::Texture::type()),(GLuint)textUnit);
	glActiveTexture(GL_TEXTURE0);

}

unsigned int SNOW::Texture::internal_id() const {
	return id_;
}

void SNOW::Texture::set_min_filter(Filter f) {
	switch (f)
	{
	case 0: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		break;
	case 1: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MIN_FILTER, F_LINEAR);
		break;
	case 2: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MIN_FILTER, F_NEAREST_MIPMAP_NEAREST);
		break;
	case 3: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MIN_FILTER, F_LINEAR_MIPMAP_NEAREST);
		break;
	case 4: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MIN_FILTER, F_NEAREST_MIPMAP_LINEAR);
		break;
	case 5: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MIN_FILTER, F_LINEAR_MIPMAP_LINEAR);
		break;
	}
}

void SNOW::Texture::set_mag_filter(Filter f) {
	switch (f)
	{
	case 0: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		break;
	case 1: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MAG_FILTER, F_LINEAR);
		break;
	case 2: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MAG_FILTER, F_NEAREST_MIPMAP_NEAREST);
		break;
	case 3: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MAG_FILTER, F_LINEAR_MIPMAP_NEAREST);
		break;
	case 4: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MAG_FILTER, F_NEAREST_MIPMAP_LINEAR);
		break;
	case 5: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_MAG_FILTER, F_LINEAR_MIPMAP_LINEAR);
		break;
	}
}

void SNOW::Texture::set_wrap_s(Wrap c){
	switch (c)
	{
	case 0: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_S, GL_NEAREST);
		break;
	case 1: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_S, F_LINEAR);
		break;
	case 2: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_S, F_NEAREST_MIPMAP_NEAREST);
		break;
	case 3: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_S, F_LINEAR_MIPMAP_NEAREST);
		break;
	case 4: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_S, F_NEAREST_MIPMAP_LINEAR);
		break;
	case 5: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_S, F_LINEAR_MIPMAP_LINEAR);
		break;
	}
	EDK3::Texture::set_wrap_s(c);

}
void SNOW::Texture::set_wrap_t(Wrap c){
	switch (c)
	{
	case 0: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_T, GL_NEAREST);
		break;
	case 1: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_T, F_LINEAR);
		break;
	case 2: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_T, F_NEAREST_MIPMAP_NEAREST);
		break;
	case 3: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_T, F_LINEAR_MIPMAP_NEAREST);
		break;
	case 4: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_T, F_NEAREST_MIPMAP_LINEAR);
		break;
	case 5: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_T, F_LINEAR_MIPMAP_LINEAR);
		break;
	}
	EDK3::Texture::set_wrap_t(c);
}
void SNOW::Texture::set_wrap_r(Wrap c){
	switch (c)
	{
	case 0: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_R, GL_NEAREST);
		break;
	case 1: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_R, F_LINEAR);
		break;
	case 2: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_R, F_NEAREST_MIPMAP_NEAREST);
		break;
	case 3: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_R, F_LINEAR_MIPMAP_NEAREST);
		break;
	case 4: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_R, F_NEAREST_MIPMAP_LINEAR);
		break;
	case 5: glTexParameteri(conver_texture_type(EDK3::Texture::type()), GL_TEXTURE_WRAP_R, F_LINEAR_MIPMAP_LINEAR);
		break;
	}

	EDK3::Texture::set_wrap_r(c);
}
void SNOW::Texture::generateMipmaps() const {

	// Generate mipmaps, by the way.
	glGenerateMipmap(conver_texture_type(EDK3::Texture::type()));

} 
