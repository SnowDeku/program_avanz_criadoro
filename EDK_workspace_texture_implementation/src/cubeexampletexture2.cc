#include <ESAT/window.h>
#include <ESAT/input.h>
#include <ESAT/draw.h>
#include <ESAT/time.h>

#include <EDK3/geometry.h>
#include <EDK3/camera.h>
#include <EDK3/drawable.h>
#include <EDK3/matdiffusetexture.h>
#include <EDK3/texture.h>
#include <EDK3/dev/gpumanager.h>
#include <EDK3/dev/opengl.h>
#include <EDK3/scoped_array.h>

#include "SNOW/texture.h"

#include <iostream>
#include <cmath>
#include "math.h"

double previus_time;

struct GameState {
	EDK3::ref_ptr<EDK3::Camera> camera;
	EDK3::ref_ptr<EDK3::Node> root;
};

unsigned int textureID;

// Texture helpers
unsigned char* loadTextureData(const char *filename, int* out_width, int* out_height);
void freeTextureData(unsigned char* data);

void prepare(GameState *state) {
	EDK3::dev::GPUManager::CheckGLError("Prepare Start");

	// Create spheres
	EDK3::ref_ptr<EDK3::Geometry> sphereGeometry;
	EDK3::CreateCube(&sphereGeometry, 1.0f, true, true);

	// Material
	EDK3::ref_ptr<EDK3::MatDiffuseTexture> diffuse_material;
	EDK3::ref_ptr<EDK3::MatDiffuseTexture::Settings> diffuse_material_settings;

	diffuse_material.alloc();
	diffuse_material_settings.alloc();

	// Nodes
	EDK3::Node *root = state->root.alloc();

	// Create a Drawable Node (Geometry+Material+Settings)
	EDK3::ref_ptr<EDK3::Drawable> drawable;

	// Material
	diffuse_material_settings.alloc();

	// Texture
	EDK3::dev::GPUManager::CheckGLError("Creating Texture");
	EDK3::ref_ptr<SNOW::Texture> texture;
	texture.alloc();
	int texture_width;
	int texture_height;
	unsigned char* pixel_data = loadTextureData("./image.tga", &texture_width, &texture_height);
	texture->init(EDK3::Texture::Type::T_2D, EDK3::Texture::Format::F_RGB, texture_width, texture_height, 1);
	texture->set_data(EDK3::Texture::Format::F_RGB, EDK3::Type::T_UBYTE, pixel_data);
	texture->set_min_filter(EDK3::Texture::Filter::F_LINEAR);
	texture->set_mag_filter(EDK3::Texture::Filter::F_LINEAR);
	texture->generateMipmaps();
	EDK3::dev::GPUManager::CheckGLError("Finishing Creating Texture");
	
	diffuse_material_settings->set_texture(texture.get());

	// Graphic
	drawable.alloc();
	drawable->set_geometry(sphereGeometry.get());
	drawable->set_material(diffuse_material.get());
	drawable->set_material_settings(diffuse_material_settings.get());

	// Transform
	drawable->set_position(0.0f, 0.0f, 0.0f);
	drawable->set_scale(0.5f, 0.5f, 0.5f);
	drawable->set_HPR(360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX, 360.0f*rand() / RAND_MAX);
	root->addChild(drawable.get());

	// Color 
	float green_color[] = { 130 / 255.0f, 171 / 255.0f, 70 / 255.0f, 1.0f };
	diffuse_material_settings->set_color(green_color);

	// Create a Camera
	state->camera.alloc();
	float pos[] = { 120,140,120 };
	float view[] = { -120,-140,-120 };
	state->camera->set_position(pos);
	state->camera->set_view_direction(view);
	state->camera->setupPerspective(70, 1280.0f / 720.0f, 1.0f, 1500.0f);
	state->camera->set_clear_color(0.99f, 0.99f, 0.99f, 1.0f);
	EDK3::dev::GPUManager::CheckGLError("Prepare END");
}

void render_function(GameState *state) {

	// Update
	state->root->set_rotation_y(5.0f * ESAT::Time() / 1000.0f);

	// For Every frame... determine what's visible:
	state->camera->doCull(state->root.get());

		// Render
	EDK3::dev::GPUManager::CheckGLError("begin Render-->");
	state->camera->doRender();
	EDK3::dev::GPUManager::CheckGLError("end Render-->");

	// -- Orbital camera:
	double mx = ESAT::MousePositionX();
	double my = ESAT::MousePositionY();
	double p = sin(-my / 200) * 220;
	float zoom_factor = 0.014f;
	float pos[] = {
		(float)(p*cos(mx / 100)) * zoom_factor,
		(float)(cos(-my / 200) * 220) * zoom_factor,
		(float)(p*sin(mx / 100)) * zoom_factor };
	float view[] = { -pos[0],-pos[1],-pos[2] };
	state->camera->set_position(pos);
	state->camera->set_view_direction(view);
}

int ESAT::main(int argc, char **argv) {

	// State
	GameState state;
	ESAT::WindowInit(1280, 720);
	ESAT::DrawSetTextFont("test.ttf");
	ESAT::DrawSetTextSize(18);
	ESAT::DrawSetFillColor(253, 255, 255, 128);
	prepare(&state);

	// Main loop
	while (ESAT::WindowIsOpened() && !ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {

		double currentTime = RawTime();
		double delta_time = (currentTime - previus_time) / 1000.0f;
		previus_time = currentTime;

		render_function(&state);
		ESAT::DrawBegin();
		ESAT::DrawText(10, 20, "equalizer");
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	return 0;
}
