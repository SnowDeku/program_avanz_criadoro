#version 330

uniform float u_time;
uniform vec3 u_view_pos; //view position
uniform mat4 u_m_matrix; //model
uniform mat4 u_vp_matrix; // view projection

layout (location=0) in vec3 a_position;
layout (location=1) in vec3 a_normal;

out vec3 normal;

void main()
{
    gl_Position = u_vp_matrix * u_m_matrix * vec4(a_position, 1.0);
    normal = a_normal * vec3(u_m_matrix);      
 }
