#version 330
 
 out vec4 fragColor;
 
 in vec3 normal;

 uniform vec3 u_color;
 
void main()
{ 
	//Set a position for the ligth
	vec3 dirligth = vec3(1,1,0);
	//Set Color
	vec3 colorligth  = vec3(2.0f,1.0f,0.0f);
	
	float diffuse_ligth = max(dot(dirligth,normal),0.0f);

	colorligth *= diffuse_ligth;

	fragColor = vec4(colorligth, 1.0);  
}
