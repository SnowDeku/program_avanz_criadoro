#include "stdio.h"
#include "winsock2.h"

int main(){

struct sockaddr_in ip;
SOCKET sock, sock_temp[4];
char buffer[4096];
memeset(buffer,0,4096);

WSADATA wsa;
WSAStartup(MAKEWORD(2,0),&wsa);

sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
ip.sin_family = AF_INET ;
ip.sin_port= hton(80);
ip.sin_addr = inet_addr("");

bind(sock,(SOCKADDR*)&ip,sizeof(ip));

listen(sock,SOMAXCONN);

sock_temp[0] = accept(sock,NULL,NULL);

recv(sock_temp[0],buffer,sizeof(buffer),0);

prtinf("\nDatos : %s", buffer);

return 0;
}

/*
* Cabezera
* HTTP/1.1 200 OK \r\nContent-Type:text/html\r\nContent-Length:1000\r\n\r\n Aqui detras estaria el fichero html
*/