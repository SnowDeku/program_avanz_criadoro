#include "stdio.h"
#include "winsock2.h"
#include "string.h"
int piedra_papel_tijera(char player1[256],char player2[256]){

  // Draws
  if(strcmp(player1,"piedra") && strcmp(player2,"piedra")) return 2;

  if(strcmp(player1,"papel") && strcmp(player2,"papel"))return 2;

  if(strcmp(player1,"tijera") && strcmp(player2,"tijera"))return 2;

  //Player 1 wins
  if(strcmp(player1,"piedra") && strcmp(player2,"tijera")) return 1;

  if(strcmp(player1,"papel") && strcmp(player2,"piedra")) return 1;

  if(strcmp(player1,"tijera") && strcmp(player2,"papel")) return 1;

  //Player 2 wins
  if(strcmp(player2,"piedra") && strcmp(player1,"tijera")) return 3;

  if(strcmp(player2,"papel") && strcmp(player1,"piedra")) return 3;

  if(strcmp(player2,"tijera") && strcmp(player1,"papel")) return 3;

}
int main(int argc, char ** argv){

    char buffer[256];
    int exit = 1;
    int score_player1;
    int score_player2;

    char bufferplayer1[256];
    char bufferplayer2[256];

    WSADATA wsa;
    //sock_l -> Socket local
    SOCKET sock_l;

    struct sockaddr_in ip;

    WSAStartup(MAKEWORD(2,0),&wsa);
    sock_l = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    ip.sin_family = AF_INET;
    ip.sin_port = htons(atoi(argv[1]));
    ip.sin_addr.s_addr = inet_addr("127.0.0.1");

    //Conectamos al servidor que hemos puesto como parametro
    if(connect(sock_l,(SOCKADDR*)&ip,sizeof(ip))){
        printf("\n Error conexion\n");
        return 33;
    }
   printf("\n Conexion con el servidor realizada de forma satisfactoria\n");

    while(exit == 1){

    printf("\n Cliente: ");
    gets(buffer);
    //Enviamos lo que tenemos en el buffer
    send(sock_l, buffer, sizeof(buffer), 0);
    if(strcmp(buffer,"exit") == 0){
        exit= 0;
    }
    strcpy(bufferplayer1,buffer);
    memset(buffer,0,256);
    //Esperamos a que el servidor nos responda y escribimos 
    recv(sock_l,buffer,sizeof(buffer),0);
    printf("\nServer :%s \n",buffer);
    if(strcmp(buffer,"exit") == 0){
        exit= 0;
    }
    strcpy(bufferplayer2,buffer);
    memset(buffer,0,256);

    if(piedra_papel_tijera(bufferplayer1,bufferplayer2) == 1){
        char text[256] = {"Player 1 win"};
        send(sock_l, text, sizeof(char*strlen(text)), 0);
    }
    if(piedra_papel_tijera(bufferplayer1,bufferplayer2) == 2){
         char text[256] = {"Draw"};
         send(sock_l, text, sizeof(char*strlen(text)), 0);
    }
     if(piedra_papel_tijera(bufferplayer1,bufferplayer2) == 3){
          char text[256] = {"Player 2 win"}; 
         send(sock_l, text, sizeof(char*strlen(text)), 0);
    }

      memset(bufferplayer2,0,256);
      memset(bufferplayer1,0,256);
    }
    

    //Cerramos el sock
    closesocket(sock_l);

    WSACleanup();

    return 0;
}