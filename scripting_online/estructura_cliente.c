#include <stdio.h>
#include <winsock2.h>

struct Clientes{
    int id;
    char nombre[50];
};

int main(){
    WSADATA wsa;
    SOCKET sock,sock_c;
    struct sockaddr_in ip,ip_c;
    struct Clientes cliente;
    int size=sizeof(ip);
    WSAStartup(MAKEWORD(2,0),&wsa);
    sock=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    ip.sin_family=AF_INET;
    ip.sin_port=htons(9999);
    ip.sin_addr.s_addr = inet_addr("127.0.0.1");
    connect(sock,(SOCKADDR*)&ip,sizeof(ip));
    printf("\nIntroduce id: ");
    scanf("%d",&cliente.id);
    printf("\nIntroduce nombre: ");
    scanf("%s",cliente.nombre);
    send(sock,(char*)&cliente,sizeof(cliente),0);
    
}