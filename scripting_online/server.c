

#include "stdio.h"
#include "winsock2.h"
//#include "common_def.h"

//Numero de clientes +1


int main(int argc,char **argv){
  
  WSADATA wsa; 
  struct sockaddr_in ip, ip_c;
  SOCKET sock,sock_c;
  int size = sizeof(ip);
  char buffer[256];
  int exit = 1;

  if (WSAStartup(MAKEWORD(2,0),&wsa) != 0) { //!=0 entra en if
    printf("\nDewbe estar cargada la libreria Winsock.");
    return 2;
  }
  
                  /////////////////
                  /////  IP  //////
                  /////////////////
  
  ip.sin_family = AF_INET;//ipv4 = AF_INET
  ip.sin_port = htons(9999);//hton = host to network + short littlenendian to big endian
  /////// 1234 envian los datos de diferente manera.Por las arquitecturas.
  ////  Big endian -> 1234
  ////  Little endian -> 4321
  ip.sin_addr.s_addr = inet_addr("0.0.0.0");
  
  
                  //////////////////
                  ///// socket /////
                  //////////////////
  //SOCKET socket(type family,type socket,type proto, se puede poner auto)
  sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  
  /////////
  //// Ahora toca bindear, el socket y el puerto.
  ///
  //// bind(socket,(SOCKADDR*) ip,sizeof);
  //////////
  if (bind(sock,(SOCKADDR*)&ip,sizeof(ip)) != 0) {
    printf("\nError bind()\n");
    return 3;
  }
  
  ////int listen(socket,maximo de conexiones) reserva un puerto y 
  //todo aquella llamada que sea a nuestro puerto pasara a nuestro programa.
  // return 0 if all ok else != 0;
  
  listen(sock,SOMAXCONN);
  
  ////SOCKET accept(SOCKET,sockadd*,size);
  
  sock_c = accept(sock,(SOCKADDR*)&ip_c,&size);
  
  printf("\nNuevo cliente conenctado.\nLa ip es:%s y puerto:%d",inet_ntoa(ip_c.sin_addr),ntohs(ip_c.sin_port));
  
  
                  //////////////////
                  //// Cliente /////
                  //////////////////
  //----- Chat con posibles conexiones a espera de el primero hablar.
  while(exit == 1){

   recv(sock_c,buffer,256,0);
   printf("\nCliente: %s",buffer);
    if(strcmp(buffer,"exit") == 0){
        exit= 0;
    }
    if(exit != 0){
      memset(buffer,0,256);
      printf("\nServer : ");
      gets(buffer);
      send(sock_c,buffer,256,0);
      if(strcmp(buffer,"exit") == 0){
        exit= 0;
      }
      memset(buffer,0,256);
    }
  }
 
  WSACleanup();
  
  return 0;
}


























