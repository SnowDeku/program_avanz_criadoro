#include "stdio.h"
#include "winsock2.h"

int main(int argc, char** argv){

    WSADATA wsa;
    SOCKET sock;

    struct sockaddr_in ip;

    WSAStartup(MAKEWORD(2,0),&wsa);
    sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    ip.sin_family = AF_INET;
    ip.sin_port = htons(atoi(argv[1]));
    ip.sin_addr.s_addr = inet_addr("127.0.0.1");
    if(connect(sock,(SOCKADDR*)&ip,sizeof(ip))){
        printf("\n Error conexion\n");
        return 33;
    }
    printf("\n Conexion con el servidor realizada de forma satisfactoria\n");
    closesocket(sock);
    WSACleanup();
    return 0;
}