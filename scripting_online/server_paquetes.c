#include <stdio.h>
#include <winsock2.h>

enum {PKT_cliente,PKT_producto};

struct Producto{
    int id;
    char producto[25];
    int precio;
}
struct Clientes{
    int id;
    char nombre[50];
};
struct Paquete{
    int id;
    union data_ (Producto pro_ ,Clientes cli_);
};

int main(){
    char buffer[256];

    WSADATA wsa;
    SOCKET sock,sock_c;
    struct sockaddr_in ip,ip_c;
    struct Clientes cliente;
    int size=sizeof(ip);
    WSAStartup(MAKEWORD(2,0),&wsa);
    sock=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    ip.sin_family=AF_INET;
    ip.sin_port=htons(9999);
    ip.sin_addr.s_addr = inet_addr("0.0.0.0");
    bind(sock,(SOCKADDR*)&ip,sizeof(ip));
    listen(sock,SOMAXCONN);
    sock_c = accept(sock,(SOCKADDR*)&ip_c,&size);

    while(strcmp(buffer,"exit") !=0){

    
        recv(sock_c,(char*)&cliente,sizeof(cliente),0);
        printf("\nCliente recibido: Cliente id %d - Nombre: %s",cliente.id,cliente.nombre);
    
    }
    return 0;
    
}