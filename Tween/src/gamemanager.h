/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/



#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__ 1

#include "Class_sprt.h"
#include "Class_text.h"
#include "Class_obj.h"
#include "Class_button.h"
#include "ESAT/window.h"
#include "ESAT/input.h"

class Gamemanager {


public:
Gamemanager();
~Gamemanager();

//Save all the needed values 
 void Start();

//Where we change all the values
 void Update();

//Where we show the result of the values and save them
void Late_Update();

//Free all the variables we used 
void End();

protected:
Object *isaac;
Text *text;
Button *play;

private:


};

#endif