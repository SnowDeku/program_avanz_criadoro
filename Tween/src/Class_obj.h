/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/

#ifndef __OBJECT_H__
#define __OBJECT_H__ 1

#include "ESAT/sprite.h"
#include "ESAT/draw.h"
#include "ESAT/input.h"

#include <stdio.h>
#include <stdlib.h>


struct color {
   unsigned short int  r,g,b,a;
};

class Object {

    public:
    //Public Variables 
      

    //Constructor
    Object(int id,ESAT::Vec3 position);

    //Destructor virtual
    virtual ~Object();

    //Functions non-virtual
    ESAT::Vec3 Init_Vec3(float x = 0,float y = 0,float z = 0);

    void set_color(unsigned short int  r,unsigned short int  g,unsigned short int  b,unsigned short int  a);
    //Functions virtual
    virtual void draw();
    virtual void new_pos(float x,float y,float z);
    virtual void get_sprite(char *path);
    virtual bool button_on(ESAT::Vec3 mouse);
    virtual void Update();

    protected:
    int id_;
    ESAT::Vec3 transform_;
    struct color obj_color_;
    bool enable_;

    private:

};

#endif