/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/
#ifndef __TEXT_H__
#define __TEXT_H__ 1

#include "string.h"

#include "Class_obj.h"


class Text : public Object{

    public:
    //Constructor 
    Text(int id,ESAT::Vec3 position={0,0,0},float size= 15);

    //Destructor with override
   
    virtual ~Text() override;

    //Non-virtual Functions
    void load_font(const char *string);
    void set_text(char *string);
    void set_size(float size);

    //Virtual Functions
    void draw()override;
    
    protected:
    char *string_;
    float size_;


    private:



};

#endif