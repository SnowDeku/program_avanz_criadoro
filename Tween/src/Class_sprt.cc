/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/
#include "Class_sprt.h"
//Constructor
Sprite::Sprite(int id ,ESAT::Vec3 position) : Object(id,position){

    id_ = id;
    transform_.x = position.x;
    transform_.y = position.y;
    transform_.z = position.z;
    enable_ = true;
}   

//Destructor
Sprite::~Sprite(){

   ESAT::SpriteRelease(handle_);
}

//Functions non-virtual
  void Sprite::get_sprite(char *path){
     handle_ = ESAT::SpriteFromFile(path);
  }

//Functions virtual
void Sprite::draw(){
    if(enable_ == true ){
            ESAT::DrawSprite(handle_,transform_.x,transform_.y);
    }
}