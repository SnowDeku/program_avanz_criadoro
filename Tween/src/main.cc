/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/
#include "gamemanager.h"


int ESAT::main(int argc, char **argv)
{

    ESAT::WindowInit(1200, 800);
    ESAT::WindowSetMouseVisibility(true);
    ESAT::DrawSetTextFont("bit.ttf");
    Gamemanager *game = new Gamemanager();

    //Start saving all the memory and thing we are going to use and put some data
    game->Start();
    
    while (!ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape) && ESAT::WindowIsOpened()){    

        ESAT::DrawBegin();
		ESAT::DrawClear(0, 0, 0); //Cleaner y color of the background in rgb.

        //Updates
        game->Update();
        game->Late_Update();

		ESAT::DrawEnd();
        ESAT::WindowFrame();
    }
    // Free all the memory at the end
    game->End();

 
    ESAT::WindowDestroy();

    return 0;
}