/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/
#include "Class_text.h"


//Constructor
Text::Text(int id ,ESAT::Vec3 position,float size) : Object(id,position),size_(size){

    id_ = id;
    transform_.x = position.x;
    transform_.y = position.y;
    transform_.z = position.z;
    size_ = size;
    enable_ = true;
}

//Destructor
Text::~Text(){
	//Manera de "borrar todo lo que hay dentro de string en vez de hacer el free ya que no funciona
	int size = strlen(string_);
	string_ = (char*) calloc(size , sizeof(char));
	*string_ = NULL;
}

//Save the font we going to use
void Text::load_font(const char *string){
    ESAT::DrawSetTextFont(string);
}


 void Text::set_text(char *string){
    int size = strlen(string);
    string_ = (char*)malloc(size*sizeof(char));
    strcpy(string_,string);
    
 }

void Text::set_size(float size){
    size_ = size;
}


 void Text::draw(){
     if(enable_ == true){
     ESAT::DrawText(transform_.x,transform_.y,string_);
     }
 }
