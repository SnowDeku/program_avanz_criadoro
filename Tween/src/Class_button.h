/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/

#ifndef __BUTTON_H__
#define __BUTTON_H__ 1

#include "Class_sprt.h"


class Button : public Sprite {

    public:

    //Constructor
    Button(int id,ESAT::Vec3 position);

    //Destructor
    virtual ~Button() override;

    //Functions non-virtual
    void shape_button();
    void show_shape();
    //Functions virtual
    virtual bool button_on(ESAT::Vec3 mouse) override;


    protected:
    bool pressed_;
    
};

#endif
