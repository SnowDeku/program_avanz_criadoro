/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/

#include "Class_button.h"

//Constructor
Button::Button(int id ,ESAT::Vec3 position) : Sprite(id,position){

    id_ = id;
    transform_.x = position.x;
    transform_.y = position.y;
    transform_.z = position.z;
    pressed_ = false;



}

//Destructor
Button::~Button(){


}

 //Functions non-virtual
void Button::shape_button(){


}
void Button::show_shape(){

}


//Functions virtual
bool Button::button_on(ESAT::Vec3 mouse){

  return 1;
}