/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/

#ifndef __ACTION_H__
#define __ACTION_H__ 1

#include "Class_obj.h"

class Action{

    public:
    
    //Constructor
    Action(float duration);
    Action(const Action& other);
        
    //Destructor virtual
    virtual ~Action();
    virtual void init();
  
    virtual void Update(float delta);

    virtual void Finish();

    virtual Action* Clone();

    protected:
    int id_;
    Object *target_;
    float duration_;
    float elapsed_;

    private:

};

#endif