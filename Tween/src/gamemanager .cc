/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/


#include "gamemanager.h"



Gamemanager::Gamemanager(){

}

Gamemanager::~Gamemanager(){


}

//Save all the needed values 
 void Gamemanager::Start(){
    isaac = new Sprite(2);
    text = new Text(1);
    play = new Button(3,{500,500,0});

    text->new_pos(50,50,0);
    text->set_text("Tale from the Isaac");

	text->set_color(255, 255, 255, 255);
    text->set_size(30);


    isaac->get_sprite("isaac.png");
    isaac->new_pos(300,300,0);
     
}

//Where we change all the values
 void Gamemanager::Update(){
  
}


//Where we show the result of the values and save them
void Gamemanager::Late_Update(){
     text->draw();
     isaac->draw();
    

}

//Free all the variables we used 
void Gamemanager::End(){

    delete text;
    delete isaac;

}
