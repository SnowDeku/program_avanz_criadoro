/*

@Jorge Criado Ros
@SnowDeku Library 

Lib for types List,Array* and Matrix funcs.

*/
#ifndef __BASE_H__
#define __BASE_H__ 1

#include <stdio.h>
#include <stdlib.h>

typedef struct Node_{
    float value_;
    Node_ *next;
}Node;

typedef struct List_{
    Node *head;
    Node *tail;
    unsigned int total_list;
}List;

class Base {



///-------------------------ERROR FUNCS-------------------------------------------



//---------------------------------List-----------------------------------------

//Used for create and Insert on a List.
//List = InserOnList(<List>,<value>);
List *insert_on_list(List *list,float value);

//Goes through all the list and shows it
static void show_list(List *list);

//Gives the size of the list
unsigned int size_of_list(List *list);

//Takes out one node and put the node before with the after node that we deleted
void delete_node(Node_ *node,List_ *list,float value);

//Returns a value from a specific position of the  List
float get_value_list(List *list,float position);

//Finds the node that we want to change and change it
void edit_node(List *list,float value,float editvalue);

//Copy a list to another.
void cpy_list(List *destination,List *source);


};

#endif