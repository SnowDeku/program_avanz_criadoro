/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/


#include "Class_obj.h"

 //Constructor
Object::Object(int id,ESAT::Vec3 position){
    id_ = id;
    transform_.x = position.x;
    transform_.y = position.y;
    transform_.z = position.z;

}


//Destructor virtual
Object::~Object(){ 

}

//Functions non-virtual
ESAT::Vec3 Object::Init_Vec3(float x,float y,float z){
    ESAT::Vec3 vec3;
    vec3.x = x;
    vec3.y = y;
    vec3.z = z;

return vec3;  
}
void Object::set_color( unsigned short int  r,
                        unsigned short int  g,
                        unsigned short int  b,
                        unsigned short int  a){
    obj_color_.r = r;
    obj_color_.g = g;
    obj_color_.b = b;
    obj_color_.a = a;
    ESAT::DrawSetFillColor(obj_color_.r,obj_color_.g ,obj_color_.b ,obj_color_.a );
}

//Functions virtual
void Object::draw(){

}
void Object::get_sprite(char *path){


}
bool Object::button_on(ESAT::Vec3 mouse){


return true;
}
void Object::new_pos(float x,float y,float z){
    ESAT::Vec3 position;

    position.x = x;
    position.y = y;
    position.z = z;

  transform_ = position;
}

void Object::Update(){
 if(transform_.x > 1200 || transform_.x <0){
     printf("position of %d out of screen ",id_);
 } 

}
