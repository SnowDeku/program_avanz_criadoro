/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/


#include "Class_action.h"

 //Constructor
Action::Action(float duration){
duration_ = duration;
target_ = nullptr;
elapsed_= 0.0f;
}

Action::Action(const Action& other){
    target_= other.target_;
    duration_= other.duration_;
    elapsed_ = other.elapsed_;
}


//Destructor virtual
Action::~Action(){

}

//Functions non-virtual


//Functions virtual
     void Action::init(){

     }
  
     void Action::Update(float delta){
         elapsed_ += delta;

     }

     void Action::Finish(){

     }

     Action* Action::Clone(){
         return (Action*) new Action(*this);
     }