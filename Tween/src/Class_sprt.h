/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/
#ifndef __SPRITE_H__
#define __SPRITE_H__ 1
#include "Class_obj.h"


class Sprite : public Object {

    public:
    //Constructor
    Sprite(int id,ESAT::Vec3 position={0,0,0});
    //Destructor
    virtual ~Sprite() override;
    
    //Functions non-virtual

    //Functions virtual
    void draw() override;
    void get_sprite(char *path) override;

    protected:
    ESAT::SpriteHandle handle_;
    

    private:

};

#endif