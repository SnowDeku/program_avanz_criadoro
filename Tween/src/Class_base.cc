/*

@Jorge Criado Ros
@SnowDeku Library 

Lib for types List,Array* and Matrix funcs.

*/

#include "Class_base.h"


//---------------------------------List-----------------------------------------

List *Base::list_create(){
    
    List *newlist;
    newlist = (List*) malloc(sizeof(List));

    newlist->head = NULL;
    newlist->tail = NULL;
    newlist->total_list = 0;

    return newlist;
}
//Used for create and Insert on a List.
//List = InserOnList(<List>,<value>);
void *Base::insert_on_list(List *list,float value){

    Node *newnode;
    newnode = (Node*) malloc(sizeof(Node));

    newnode->value_ = value;
    newnode->next = NULL;

    if(list->head == NULL){

     list->head = newnode;
     list->tail = newnode;
     list->total_list++;

    }else{
      //Change the tail and increase the number of nodes
       list->tail->next = newnode;
       list->tail = newnode;
       list->total_list++;
       
    }

}

//Goes through all the list and shows it
static void Base::show_list(List *list){

    Node *aux;
    aux = (Node*) malloc(sizeof(Node));
    aux = list->head;

    for(int i ; i< list->total_list : i++){
        
        

    }




}

//Gives the size of the list
unsigned int Base::size_of_list(List *list);

//Takes out one node and put the node before with the after node that we deleted
void Base::delete_node(Node *node,list *List,float value);

//Returns a value from a specific position of the  List
float Base::get_value_list(List *list,float position);

//Finds the node that we want to change and change it
void Base::edit_node(List *list,float value,float editvalue);

//Copy a list to another.
void Base::cpy_list(List *destination,List *source);


