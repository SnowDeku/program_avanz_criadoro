/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/
#ifndef __COLOR3_H__
#define __COLOR3_H__ 1

#include <stdio.h>
#include <iostream>

class Color3 {

  protected:

    int r_;
    int g_;
    int b_;


  public:
    Color3(int r,int g,int b);
    ~Color3();
   void addRGB(Color3 &c_in);

};

#endif 