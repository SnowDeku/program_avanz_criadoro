/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/


class Color4 : public Color3 {

   protected:

    int a_;

    public:

    Color4();
    ~Color4();
    void addRGBA(Color4 &c_in);

}