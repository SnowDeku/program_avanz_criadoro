#version 330
//Change all sides diferent colors
uniform float u_time;
uniform mat4 u_m_matrix;
uniform mat4 u_vp_matrix;

layout (location=0) in vec3 a_position;
layout (location=1) in vec3 a_normal;

out vec3 normal;
out vec3 position;

 vec3 t_position;
 vec3 s_position;
 vec3 c_normal;

void main() {
    
    //Transforme into a cube
    // gl_Position = u_m_matrix * vec4(a_position, 1.0f) * vec4(1.0f,1.5f,1.0f,1.0f);
    // Another way to Transforme
    // gl_Position = (u_m_matrix) * vec4(a_position.x,a_position.y*1.25,a_position.z, 1.0f);
    // Or
     t_position = a_position;
     t_position.y *=1.25;
    // gl_Position = (u_m_matrix) * vec4(t_position.y, 1.0f);
    // Scale the cube 
    s_position = t_position * 0.5;
    gl_Position = u_m_matrix * (vec4(s_position, 1.0f));

  //c_normal = a_normal;
  //c_normal += vec3(0.5f,0.5f,0.5f);

    normal = a_normal;
 }
