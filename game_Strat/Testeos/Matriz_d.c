/*

@Jorge Criado Ros
@SnowDeku Library Super


*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


int main(){

    int filas,columnas;
    int **m1,**m2;

    printf("Introduce el numero de filas: ");
    scanf("%i",&filas);

    printf("Introduce el numero de columnas: ");
    scanf("%i",&columnas);

    m1 = (int**) malloc(filas*sizeof(int*));
    if(m1 == NULL){
        printf("No se ha podido reservar la memoria");
        exit(1);
    }

    for(int i = 0; i < filas;i++){
        m1[i] = (int*) malloc(columnas*sizeof(int));
        if(m1[i] == NULL){
             printf("No se ha podido reservar la memoria");
             exit(1);
        }
    }

    for(int i = 0;i <filas ;i++){
        for(int j = 0 ; j < columnas ; j++){
            scanf("%i",&m1[i][j]);
        }
    }

    m2 =  (int**)malloc(filas*sizeof(int*));
       if(m2 == NULL){
        printf("No se ha podido reservar la memoria");
        exit(1);
    }

     for(int i = 0; i < filas;i++){
        m2[i] = (int*) malloc(columnas*sizeof(int));
        if(m2[i] == NULL){
             printf("No se ha podido reservar la memoria");
             exit(1);
        }
    }

    for(int i = 0;i <filas ;i++){
        for(int j = 0 ; j < columnas ; j++){
          m2[i][j] = m1[i][j];
          printf("%i",m2[i][j]);
        }
        printf("\n");
    }

    free(m1);
    free(m2);

    system("pause");
    return 0;
}