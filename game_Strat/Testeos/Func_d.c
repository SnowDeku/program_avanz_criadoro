/*

@Jorge Criado Ros
@SnowDeku Library Super


*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


int reservar(int filas ,int colum){

    int **mat;

    mat = (int**)malloc(filas*sizeof(int*));
    if(mat == NULL){
        printf("No se ha podido reservar memoria");
        exit(1);
    }
    for(int i  = 0; i < filas; i++){
        mat[i] = (int*)malloc(colum*sizeof(int));
          if(mat[i] == NULL){
        printf("No se ha podido reservar memoria");
        exit(1);
    }
    }

    return mat;
}

void introduce(int filas,int colum,int **mat){


    for(int i = 0;i < filas;i++){
        for(int j = 0; j < colum;j++){
            printf("Introduce el valor para el elemento[%i][%i]",i,j);
            scanf("%i",&mat[i][j]);
        }
    }


}


void comparar(int filas,int colum,int **m1,int **m2){
    int aux = 0;

    for(int i = 0;i < filas && aux == 0;i++){
        for(int j = 0; j < colum && aux == 0;j++){

            if(m1[i][j] != m2[i][j]){
                aux = 1;
            }

        }
    }

    if(aux == 0){
        printf("Las dos matrices son iguales");

    }else{
        printf("Las matrices no coinciden");
    }
}

int main(){

    int filas,colum;
    int **m1,**m2;

    printf("Introduce el numero de filas: ");
    scanf("%i",&filas);

    printf("Introduce el numero de columunas: ");
    scanf("%i",&colum);

    m1 = reservar(filas,colum);
    m2 = reservar(filas,colum);

    introduce(filas,colum,m1);
    introduce(filas,colum,m2);

    comparar(filas,colum,m1,m2);
    
    free(m1);
    free(m2);

    system("pause");
    return 0;
}