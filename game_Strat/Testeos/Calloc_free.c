/*

@Jorge Criado Ros
@SnowDeku Library Super


*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void aleatorio(int *vector,int N){
    int i;
    srand(time(NULL));

    for(i = 0;i < N; i++){
        *(vector+i) = rand() % 3;
    }
}

int main(){

    int op;
    int N = 3;
    int vector[100];
    int *d_vector;

    d_vector  = (int*) calloc(N,sizeof(int));
    if(d_vector == NULL){
        printf("No se ha podido reservar la memoria");
    }else{
        aleatorio(vector,100);
        for(int i = 0; i < 100; i++){
             op = vector[i];
            *(d_vector+op) +=1;

        }
        for(int i = 0 ; i < N ; i++){
            printf("%i, ",*(d_vector+i));
        }

    }
    //Muy importante liberar la memoria dinamica
    free(d_vector);

    system("pause");
    return 0;
}