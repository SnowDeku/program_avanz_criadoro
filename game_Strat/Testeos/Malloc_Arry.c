/*

@Jorge Criado Ros
@SnowDeku Library Super


*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Usamos esto para rellenar un array dinamico con aleatorios con N numeros
void aleatorio(int *vector,int N){
    int i;
    srand(time(NULL));

    for(i = 0;i < N; i++){
        *(vector+i) = rand() % 3;
        printf("%i, ",*(vector+i));
    }
    printf("\n");
}

int main(){
    int i,j,k;
    int N = 10;
    int *vector;

    vector = (int*) malloc(N*sizeof(int));

    if(vector == NULL){
        printf("No se ha podido reservar memoria.\n");
    }else{
        aleatorio(vector,N);
        for(i = 0; i < N ;i++ ){
            j = i+1;
            while(j < N){
                if(*(vector+i)==*(vector+j)){
                  for(k = j; k+1 < N;k++){
                     *(vector+k) = *(vector+k+1);
                  }
                 N--;
                }else{
                    j++;
                }

            }
        }
    }
    vector = (int *) realloc(vector,N*sizeof(int));
    if(vector == NULL){
            printf("No se ha podido reservar memoria.\n");
    }else{
        for(int i =0 ; i< N;i++){
            printf("%i, ",*(vector+i));
        }
        printf("\n");
    }


system("pause");
return 0;
}

