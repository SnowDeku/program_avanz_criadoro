#include <stdio.h>
#include <winsock.h>
#include <string.h>

int main(int argc,char **argv){

    WSADATA wsa;
    SOCKET sock,sock_c;
    struct sockaddr_in ip, ip_c;
    char buff[256];
    int size = 0;
    int bytes = 0 ;

    memset(buff,0,256);

    WSAStartup(MAKEWORD(2,0),&wsa);
    sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

    ip.sin_family = AF_INET;
    ip.sin_addr.s_addr = inet_addr("0.0.0.0");
    ip.sin_port = htons(atoi(argv[1]));

    bind(sock,(SOCKADDR*)&ip,sizeof(ip));
    listen(sock,SOMAXCONN);
    size = sizeof(ip);
    sock_c = accept(sock,(SOCKADDR*)&ip_c,&size);
    bytes = recv(sock_c,buff,sizeof(buff),0);

    printf("\n [%s] \n",buff);

    return 0;

}