/*

@Jorge Criado Ros
@SnowDeku 

@Avanzed Programing
*/

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "ESAT/draw.h"
#include "ESAT/window.h"
#include "ESAT/input.h"

#include "gamemanager.cc"

void endframeesat(){
     
        ESAT::WindowFrame();
        ESAT::DrawClear(0, 0, 0); //Cleaner y color of the background in rgb.
        ESAT::DrawEnd();
}



int ESAT::main(int argc, char **argv)
{

    ESAT::WindowInit(1200, 800);
     ESAT::WindowSetMouseVisibility(true);
       DrawSetTextFont("bit.ttf");
    while (!ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape) && ESAT::WindowIsOpened())
    {    
       
    Gamemanager();
    endframeesat();
    }

    ESAT::WindowDestroy();

    return 0;
}