/*

@Jorge Criado Ros
@SnowDeku Library 

This is the class of new objects
*/

#include "instance.h"


  Instance::Instance(){


  }

  Instance::~Instance(){


  }


 //Creates and give 0 to value to all of the data of the new object
  void Instance::InitGameObject(Object *object){

    object->shape.transform.x = 0.0f;
    object->shape.transform.y = 0.0f;
    object->shape.transform.z = 0.0f;

    object->shape.data = vrs.InsertOnList(object->shape.data,0);

    object->rgb.r = 255;
    object->rgb.g = 255;
    object->rgb.b = 255;
    object->rgb.a = 255;

 } 

  void Instance::InsertShapeOfObject(Object *object, float x,float y,float z){

   object->shape.data = vrs.InsertOnList(object->shape.data,x);
   object->shape.data = vrs.InsertOnList(object->shape.data,y);
   object->shape.data = vrs.InsertOnList(object->shape.data,z);
    
 }

  void Instance::InsertShapeOfObject(Object *object,float *ary){

     for(int i = 0; *(ary+i) != NULL ; i++){
         object->shape.data = vrs.InsertOnList(object->shape.data,*(ary+i));
     }

 }
  void Instance::MoveGameObject(Object *object, float x,float y,float z){

    //Use a aux to move through the list
    Node *aux;
    aux=(Node*)malloc(sizeof(Node));
    vrs.checknode(aux);
    aux =object->shape.data;

    object->shape.transform.x += x;
    object->shape.transform.y += y;
    object->shape.transform.z += z;

    while(aux != NULL){
        //We treat the List like is and  mat of x,y,z
        aux->v += x;
        aux = aux->next;
        aux->v += y;
        aux = aux->next;
        aux->v += z;
        aux = aux->next;
     }
 }

bool Instance::ButtonClick(float *ary,Vec2S pos){

    if(true == vrsmath.Square2DColision(ary,pos)){
        return true;
    }

    return false;
}

SpriteHandle Instance::SpriteSave(const char *path){

    return ESAT::SpriteFromFile(path);
}

void Instance::SpriteDraw(SpriteHandle img ,Vec2S pos){
    
    ESAT::DrawSprite(img,pos.x,pos.y);
}

 void Instance::BoxText(float size,Vec2S pos,const char *text,float blur){

    ESAT::DrawSetTextSize(size);
    ESAT::DrawSetTextBlur(blur);
    ESAT::DrawText(pos.x,pos.y,text);

}

void Instance::BoxText(float size,float x,float y,const char *text,float blur){

    ESAT::DrawSetTextSize(size);
    ESAT::DrawSetTextBlur(blur);
    ESAT::DrawText(x,y,text);
}
