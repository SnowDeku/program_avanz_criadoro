/*

@Jorge Criado Ros
@SnowDeku Library


REACER TODO CON LA NUEVA CLASS DE GAMEOBJECT


*/
#include "snowmath.h"

void Snowmath::Data2D(Shape *Figure,float radio,int numSides,float center_x,float center_y){

  float radians;

  Figure->transform.x = center_x;
  Figure->transform.y = center_y;
  Figure->radio = radio;

  for (int i = 0; i < 360; i+=(360/numSides)){

  	radians = (i*(2*N_PI))/360;

	Figure->data = vrs.InsertOnList((Figure->data),(radio * cos(radians) + center_x));
	Figure->data = vrs.InsertOnList((Figure->data),(radio * sin(radians) + center_y));

  }

}

// This func will give an arry with the first 2 value are the center an then the points of the shape 
float *Snowmath::Data2D(float radio,float posx, float posy,unsigned short int numSides){

	float radians;
	float *shape;
	int posary = 0 ;

	shape = vrs.InitArry(numSides);
	

  for (int i = 0; i < 360; i+=(360/numSides)){

  	radians = (i*(2*N_PI))/360;

	vrs.InsertOnArry(&(*shape),posary,radio * cos(radians) + posx);
	posary++;
	vrs.InsertOnArry(&(*shape),posary,radio * sin(radians) + posy);
	posary++;

  }
 return shape;
}
/*
void Snowmath::Draw2D(Object Figure, unsigned int numSides){

	int count = 0;

	if((numSides*2) != vrs.SizeofList(Figure.shape.data)){
		printf("The number of sides is diferent as the number of points ");
	}else{

	  ESAT::DrawSetStrokeColor(Figure.rgb.r,Figure.rgb.g,Figure.rgb.b,Figure.rgb.a);
	
	for (int j = 0; j < numSides*2; j+=4)
		{
		 ESAT::DrawLine(vrs.GetValorFromList(*Figure.shape.data,j),
		 				vrs.GetValorFromList(*Figure.shape.data,(j+1)),
						vrs.GetValorFromList(*Figure.shape.data,(j+2)),
						vrs.GetValorFromList(*Figure.shape.data,(j+3)));

		}
	    ESAT::DrawLine(vrs.GetValorFromList(*Figure.shape.data,(numSides*2)),
					   vrs.GetValorFromList(*Figure.shape.data,(numSides*2-1)),
					   vrs.GetValorFromList(*Figure.shape.data,0),
					   vrs.GetValorFromList(*Figure.shape.data,1));
	}

}

void Snowmath::Draw2D(float posx,float posy,float wide,unsigned short int numsides){

	float *shape;

	shape = vrs.InitArry(numsides);
	shape = Data2D((wide/2),posx,posy,numsides);
		  
	ESAT::DrawSetStrokeColor(255,255,255,255);
	ESAT::DrawPath(shape,numsides);

}
*/
///--------Colliders Funcs

bool Snowmath::Circle2DColision(Object Figure,Vec2S point2D){

	float r,sumax,sumay;

		bool entered =false;
 	 sumax= Figure.shape.transform.x-point2D.x;
 	 sumax*=sumax;
 	 sumay= Figure.shape.transform.y-point2D.y;
 	 sumay*=sumay;

 	 r=sqrt(sumax+sumay);

 	 if(r <= Figure.shape.radio){
 	 	entered =true;
 	 }

 	 return entered;
}

float Snowmath::Sign(Vec2S p1, Vec2S p2, Vec2S p3){

  return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

bool Snowmath::IsPointInTri(Vec2S pt, Vec2S v1, Vec2S v2, Vec2S v3){
  bool b1, b2, b3;

  b1 = Sign(pt, v1, v2) < 0.0f;
  b2 = Sign(pt, v2, v3) < 0.0f;
  b3 = Sign(pt, v3, v1) < 0.0f;

  return ((b1 == b2) && (b2 == b3));
}

bool Snowmath::Triangle2DColision(float *ary,Vec2S point2D){

Vec2S shape[3];

shape[0].x = *(ary);
shape[0].y = *(ary+1);

shape[1].x = *(ary+2);
shape[1].y = *(ary+3);

shape[2].x = *(ary+4);
shape[2].y = *(ary+5);

		return IsPointInTri(point2D,shape[0],shape[1],shape[2]);
}

bool Snowmath::Square2DColision(float *ary,Vec2S point2D){

Vec2S shape[4];

shape[0].x = *(ary);
shape[0].y = *(ary+1);

shape[1].x = *(ary+2);
shape[1].y = *(ary+3);

shape[2].x = *(ary+4);
shape[2].y = *(ary+5);

shape[3].x = *(ary+6);
shape[3].y = *(ary+7);

		if(true == IsPointInTri(point2D,shape[0],shape[1],shape[2])  &&
			true ==  IsPointInTri(point2D,shape[2],shape[3],shape[0])){
			return true;
		}else{
			return false;
		}
}


