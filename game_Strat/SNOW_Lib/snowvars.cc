/*

@Jorge Criado Ros
@SnowDeku Library 

This is the class of new types of var
*/
#include "snowvars.h"



///-------------------------ERROR FUNCS-------------------------------------------

//Errors of secure memory for arrays 
void Snowvars::checkfloatarry(float *ary){
    if(ary == NULL){
        printf("Can not secure any memory");
        exit(1);
    }
}

//Errors of secure memoru for mats
void Snowvars::checkmat(float **mat){
    if(mat == NULL){
        printf("Can not secure any memory");
        exit(1);       
    }
}

//Errors of secure memory for nodes
void Snowvars::checknode(Node *var){
    if(var == NULL){
        printf("Can not secure any memory");
        exit(1);
    }
}

//---------------------------------List-----------------------------------------

//Used for create and Insert on a List.
//List = InserOnList(<List>,<value>);
Node *Snowvars::InsertOnList(Node *List,float value){

    Node *New_List;
    New_List = (Node*) malloc(sizeof(Node));
    checknode(New_List);

    New_List->v = value;
    New_List->next = NULL;
    //First node input
    if(List == NULL){
        return New_List;
    }else{
        Node *Aux;
        Aux = (Node*)malloc(sizeof(Node));
        checknode(Aux);
        Aux = List;
        //Find the last node
        while(Aux->next != NULL){
            Aux = Aux->next;
        }
        //Insert new node
        Aux->next = New_List;
        return List;
    }
      
}

//Goes through all the list and shows it
void Snowvars::ShowList(Node *List){

    Node *Aux;
    Aux = (Node*)malloc(sizeof(Node));
    checknode(Aux);

    Aux = List;
    if(Aux == NULL){
        printf("\nList is empty !\n\n");
    }else{
        printf("\nValues of the List: ");
         while(Aux->next != NULL){         
             printf("%f, ",Aux->v);
             Aux = Aux->next; 
         };
         printf("%f, ",Aux->v);
    }
}


//Gives the size of the list
unsigned int Snowvars::SizeofList(Node *List){

    Node *Aux;
    Aux = (Node*)malloc(sizeof(Node));
    checknode(Aux);
    int count = 0 ;

    Aux=List;
        if(Aux == NULL){
        printf("\nList is empty !\n\n");
        return 0;
    }else{
         while(Aux->next != NULL){         
             count++;
             Aux = Aux->next; 
         };
         count++;
         return count;
    }

}
//Takes out one node and put the node before with the after node that we deleted
void Snowvars::DeleteNode(Node *List,float value){
    
    Node *finder;
    bool done = false;
    finder = (Node*)malloc(sizeof(Node));
    checknode(finder);
    finder = List;
    
    while(done == false && finder != NULL){
        if(finder->next->v != value){
            finder = finder->next;
        }else{
            done = true;
        }
    }
    if(finder == NULL){
        printf("Value has not been founded ! \n");
    }else{
        Node *delnode;
        delnode = (Node*)malloc(sizeof(Node));
        checknode(delnode);
        delnode = finder->next;
        finder->next = finder->next->next;
        free(delnode);
    }
}
float GetValorFromList(Node *List,float position){
    int count = 0;
    float value;

    Node *finder;
    finder = (Node*)malloc(sizeof(Node));
    finder = List;

    while(count != position){
        finder = finder->next;
    }
    value = finder->v;

    return value;
}
//Finds the node that we want to change and change it
void Snowvars::EditNode(Node *List,float value,float editvalue){

    Node *finder;
    bool done = false;
    finder = (Node*)malloc(sizeof(Node));
    checknode(finder);
    finder = List;
    
    while(finder->v != value && finder != NULL){
        finder = finder->next;
    }
   
    if(finder == NULL){
        printf("Value has not been founded ! \n");
    }else{
        finder->v = editvalue;
    }
}

//---------------------------Arry var --------------------------------------------

// ary = InitArry(<Size of the array>); 
float *Snowvars::InitArry(unsigned short int size){

    float *ary;
    ary = (float*)malloc(size*sizeof(float));
    checkfloatarry(ary);
    return ary;
}

//Will put the number on the position chosen
//InsertOnArry(<array>,<position>,<value>);
void Snowvars::InsertOnArry(float *ary,unsigned short int position,float value){
    if(*(ary+position) != NULL){
        *(ary+position) = value;
    }else{
        printf("That position does not exist");
    }     
}

// Just shows the dinamic array
// ShowArry(<array>,<Size>);
void Snowvars::ShowArry(float *ary,unsigned short int size){
      for(int i = 0; i < size; i++){
        printf("%f, ",*(ary+i));
    }
}

//------------------------Matrix var-----------------------------------------------


float **Snowvars::InitMat(unsigned short int rows){
    float **mat;

    mat = InitMat(rows,rows);

    return mat;
}

// m1 = InitMat(<rows>,<columns>);
float **Snowvars::InitMat(unsigned short int rows,unsigned short int column){

    float **mat;
    //Save first and array for rows 
    mat = (float**)malloc(rows*sizeof(float*));
    //Check if there is enougth memory
    checkmat(mat);
    //For each colum we save one by one all the memory
    for(int i  = 0; i < rows; i++){
        mat[i] = (float*)malloc(column*sizeof(float));
        checkfloatarry(mat[i]);
  }

  return mat;
}



// InsertOnMat(<mat>,<rows>,<columns>,<ary>);
void Snowvars::InsertOnMat(float **mat,unsigned short int rows,unsigned short int column,float *ary){
    //Used to move through all the array
    int Totalnum  = 0;
    //Cambiaremos para que acepte un array y que lo vaya metiendo dentro de la matriz
    for(int i = 0;i < rows;i++){
        for(int j = 0; j < column;j++){
            mat[i][j] = *(ary+Totalnum);
            Totalnum++;
        }
    }
}

//Goes through all the matrix and shows it
// ShowMat(<mat>,<rows>,<columns>);
void Snowvars::ShowMat(float **mat,unsigned short int rows,unsigned short int column){
  for(int i = 0;i < rows;i++){
        for(int j = 0; j < column;j++){
            printf("[%f] ",mat[i][j]);
        }
        printf("\n");
    }
}
float Snowvars::LookInMat(float **mat,unsigned short int rows,unsigned short int column,float valor){

 bool finded = false;
 int i,j;

    for( i = 0;i < rows && finded == false ;i++){
        for( j = 0; j < column && finded == false ;j++){
            if (mat[i][j] == valor){
                finded = true;
            }
        }
    }
 
 return mat[i][j];
}
//----------------------------------------------------------------------------
