/*

@Jorge Criado Ros
@SnowDeku Library 

This is the class of new objects


*/

#ifndef __INSTANCE_H__
#define __INSTANCE_H__ 1

#include "snowmath.h"

typedef void* SpriteHandle;

 class Instance{

    public:


    Instance();
    ~Instance();

    void BoxText(float size,Vec2S pos,const char *text,float blur);

    void BoxText(float size,float x,float y,const char *text,float blur);

    void InitGameObject(Object *object);

    void InsertShapeOfObject(Object *object, float x,float y,float z);
    
    void InsertShapeOfObject(Object *object,float *ary);

    void MoveGameObject(Object *object, float x,float y,float z);

    bool ButtonClick(float *ary,Vec2S pos);
    
    ESAT::SpriteHandle SpriteSave(const char *path);

    void SpriteDraw(SpriteHandle img ,Vec2S pos);


    private:
    Snowvars vrs;
    Snowmath vrsmath;

};

 #endif