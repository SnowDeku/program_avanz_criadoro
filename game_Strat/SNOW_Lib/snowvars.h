/*

@Jorge Criado Ros
@SnowDeku Library 

This is the class of new types of var


*/

#ifndef __SNOWVARS_H
#define __SNOWVARS_H  1

#include <stdio.h>
#include <stdlib.h>

struct Node{
    float v;
    Node *next;
};


class Snowvars{

public:

Snowvars();
~Snowvars();

float *ary;
float **m1,**m2;

///-------------------------ERROR FUNCS-------------------------------------------

//Errors of secure memory for arrays 
void checkfloatarry(float *ary);

//Errors of secure memoru for mats
void checkmat(float **mat);

//Errors of secure memory for nodes
void checknode(Node *var);
//---------------------------------List-----------------------------------------

//Used for create and Insert on a List.
//List = InserOnList(<List>,<value>);
Node *InsertOnList(Node *List,float value);

//Goes through all the list and shows it
void ShowList(Node *List);

//Takes out one node and put the node before with the after node that we deleted
void DeleteNode(Node *List,float value);

unsigned int SizeofList(Node *List);

//Finds the node that we want to change and change it
void EditNode(Node *List,float value,float editvalue);

float GetValorFromList(Node List,float position);

//---------------------------Arry var --------------------------------------------

// ary = InitArry(<Size of the array>); 
float* InitArry(unsigned short int size);

//Will put the number on the position chosen
//InsertOnArry(<array>,<position>,<value>);
void InsertOnArry(float *ary,unsigned short int position, float value);

// Just shows the dinamic array
// ShowArry(<array>,<Size>);
void ShowArry(float *ary,unsigned short int size);

//------------------------Matrix var-----------------------------------------------

// m1 = InitMat(<rows>,<columns>);
float **InitMat(unsigned short int rows);
float **InitMat(unsigned short int rows,unsigned short int column);


// InsertOnMat(<mat>,<rows>,<columns>,<ary>);
void InsertOnMat(float **mat,unsigned short int rows,unsigned short int column,float *ary);

//Goes through all the matrix and shows it
// ShowMat(<mat>,<rows>,<columns>);
void ShowMat(float **mat,unsigned short int rows,unsigned short int column);
float LookInMat(float **mat,unsigned short int rows,unsigned short int column,float valor);
//----------------------------------------------------------------------------
private:


};

#endif