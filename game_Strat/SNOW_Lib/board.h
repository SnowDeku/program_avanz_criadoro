/*

@Jorge Criado Ros
@SnowDeku Library


Class BOARD


*/

#ifndef __BOARD_H__
#define __BOARD_H__ 1


#include "instance.h"

    struct BoardDraw{
        //used for draw this matrix 
        float **material;
         float mat;
    };
 
    struct Clickon{
        bool on;
        float mat_x,mat_y;
    };



class Board {

    public:
   

     //Returnt the size of the mat given
    float *SizeMat(float **m);

     //Place on the board all 0
    //InitBoard(<matrix>);
    void InitBoard(float **m);

    //Reserve space memory
    //CreateBoard(<rows || AxA>,<cols>);
    //AxA means that if rows and cols are the same there is no need to put the second paremeter
    float **CreateBoard(unsigned short int size_rows,unsigned short int size_cols);
    float **CreateBoard(unsigned short int size_rows);
   
    //Fills the board with the ary given.
    void FillBoard(float **m,float *ary);

    //EditBoard(<matrix>,<ary>,<rows || AxA>,<cols>);
    void EditBoard(float **m,float value,unsigned short int pos_row,unsigned short intpos_cols);

    //Look for a value int the mat given
    float SearchOnBoard(float **m,float value);

    //Will return if is inside and there the mat is;
    Clickon ClickOnBoard(float **m,float posx,float posy);

       /*
    check the area of the mat and see if the position is inside then look 
    for the exact  place of the mat.
               \
              ^ \__________
              | |         |
              | |         |
              | |         |
              v |_________|
                           \
                <---------->\
    */
    void DrawBoard(float **m,int wide,float x,float y);

    //~DestroyBoard();
    private:
    Instance vrsins;
    Snowmath vrsmath;
    Snowvars vrs;
    int wide_of_board;
    BoardDraw boarddraw;


};




#endif