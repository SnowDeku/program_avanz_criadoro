/*

@Jorge Criado Ros
@SnowDeku Library


REACER TODO CON LA NUEVA CLASS DE GAMEOBJECT

COMENTAR AQUI TODO EL CODIGO

*/

#ifndef __SNOWMATH_H__
#define __SNOWMATH_H__ 1

#include <math.h>
#include "ESAT/sprite.h"
#include "ESAT/draw.h"

#include "snowvars.h"

const float N_PI = 3.14159265358979323846;

const unsigned short int Square = 4;
const unsigned short int Triangle = 3;
const unsigned int Circle = 360;

struct Transform{
       float x,y,z;
    };
    
struct Shape{
       Transform transform;
       Node *data;
       float radio;
    };

struct RGB{
        unsigned short int r;
        unsigned short int g;
        unsigned short int b;
        unsigned short int a;
    };

struct Object{
        Shape shape;
        RGB rgb;
    };
struct Mouse{
    float x,y;

};
struct Vec2S{
    float x,y;
};

class Snowmath {

public:

//Generates the points for the shape we want 
void Data2D(Shape *Figure,float radio,int numSides,float center_x,float center_y);

// This func will give an arry with the first 2 value are the center an then the points of the shape 
float *Data2D(float radio,float posx,float posy,unsigned short int numsides);

//Uses draw line by line
/*
void Draw2D(struct Object Figure,unsigned int numSides);
//Uses the Draw path
/*
void Draw2D(float posx,float posy,float wide,unsigned short int numsides);
*/
bool Circle2DColision(Object Figure,Vec2S point2D);

float Sign(Vec2S p1, Vec2S p2, Vec2S p3);

bool IsPointInTri(Vec2S pt, Vec2S v1, Vec2S v2, Vec2S v3);

bool Triangle2DColision(float *ary,Vec2S point2D);

bool Square2DColision(float *ary,Vec2S point2D);

~Snowmath();
private:

Snowvars vrs;

};

#endif

