#version 330
 
 out vec4 fragColor;
 
in vec3 normal;
in vec4 frag_position;

void main() { 

	vec3 finalcolor = normal;

	if(frag_position.x < 0){
		finalcolor = vec3(1,0,0);
	}

	fragColor = vec4(finalcolor, 1.0);
	
 }
