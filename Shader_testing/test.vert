#version 330

uniform float u_time;
uniform mat4 u_m_matrix;
uniform mat4 u_vp_matrix;

layout (location=0) in vec3 a_position;
layout (location=1) in vec3 a_normal;

out vec3 normal;
out vec4 frag_position;

out vec3 position_;
out float time_;

void main() {
   
   time_ = u_time;
    position_ = a_position;

    gl_Position = u_vp_matrix * u_m_matrix * vec4(a_position, 1.0);
    normal = a_normal;

    frag_position = u_vp_matrix * u_m_matrix * vec4(a_position, 1.0);
 }
