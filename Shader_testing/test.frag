#version 330
 
 out vec4 fragColor;
 
in vec3 normal;
in vec4 frag_position;

in vec3 position_;
in float time_;

void main() { 	
  	vec3 center = vec3(0,0,0);

	float radius_in = 0.25 * abs(sin(time_))+0.5;
	float radius_out = 0.4 * abs(sin(time_))+0.5;

	vec3 finalcolor = normal;
	float distance_ = distance(position_,center);


	if (distance_ > radius_in && distance_ < radius_out){
		finalcolor = vec3(1,0.25,0.5);
	}
	
	fragColor = vec4(finalcolor, 1.0);
	
 }
