/*
Programacion Avanzada 
Jorge Criado Ros

*/

#ifndef __CHARACTER_H__
#define __CHARACTER_H__ 1

class Character{


public:
Character();
~Character();

//TO DO
unsigned int getLife();
unsigned int getAtack();
unsigned int getDefense();
unsigned int getMana();

protected:
//TO DO
void setLife(unsigned int life);
void setAtack(unsigned int atack);
void setDefense(unsigned int defense);
void setMovement(unsigned int movement);
void setSp_atack(unsigned int sp_atack);
void setExperience(unsigned int experience);
void setInitPosition(unsigned int pos_x,unsigned int pos_y);
void Enabled(bool active);
//vars
unsigned int life_;
unsigned int atack_;
unsigned int defense_;
unsigned int movement_;
unsigned int sp_atack_;
unsigned int experience_;

bool enabled_;
unsigned int pos_x_;
unsigned int pos_y_;

//virtual
virtual void Movement() = 0;


//non_virtual




private:



};

#endif