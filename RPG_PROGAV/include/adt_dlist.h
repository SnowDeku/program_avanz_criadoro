// adt_vector.h : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#ifndef __ADT_DLIST_H__
#define __ADT_DLIST_H__


#include "memory_node.h"


typedef struct adt_dlist_s{

	MemoryNode *head_;
	MemoryNode *tail_;
	u16 total_elem_;
} DList;

DList* DLIST_Create();
static s16 DLIST_Init(DList *dlist);
static bool isEmpty(DList *dlist);
static u16 Length(DList *dlist);
static s16 InsertFirst(DList *dlist,void *value,u16 bytes);
static s16 InsertLast(DList *dlist,void *value,u16 bytes);
static s16 Insert(DList *dlist, void *value, u16 position_,u16 bytes);
static MemoryNode **Head(DList *dlist);// Head: returns a reference to the first node
static MemoryNode *ExtractFirst(DList *dlist);
static MemoryNode *ExtractLast(DList *dlist);
static MemoryNode *Extract(DList *dlist, u16 position_);
static u16 Concat(DList* src,DList* dest);
u16 List_Traverse(DList *dlist, void(*callback) (MemoryNode *));// traverses a list and applies a callback to each node
static s16 Print(DList *dlist);
#endif //__ADT_DLIST_H__