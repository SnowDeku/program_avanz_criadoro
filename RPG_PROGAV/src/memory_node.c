// memory_node.c : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#include "../include/memory_node.h"


static MemoryNode* MEMNODE_Create();
static s16 MEMNODE_Init(MemoryNode *node);
static void* MEMNODE_Data(MemoryNode *node); // returns a reference to data_
static u16 MEMNODE_Size(MemoryNode *node); // returns data size
static s16 MEMNODE_setData(MemoryNode *node, void *src, u16 bytes);
static s16 MEMNODE_MemSet(MemoryNode *node, u8 value);
//-----------------------------------------------------------------------------Create 2 different MemCopy
static s16 MEMNODE_MemCopy(MemoryNode *node, void *src, u16 bytes);
static s16 MEMNODE_MemCopy2(MemoryNode *src,MemoryNode *dest);
//-----------------------------------------------------------------------------Create 2 different MemConcat
static s16 MEMNODE_MemConcat(MemoryNode *node, void *src, u16 bytes);
static s16 MEMNODE_MemConcat2(MemoryNode *node, MemoryNode *dest);

static void MEMNODE_Print(MemoryNode *node);

struct memory_node_ops_s memory_node_ops =
 {
	.Init = MEMNODE_Init,
	.Data = MEMNODE_Data,
	.Size = MEMNODE_Size,
	.setData = MEMNODE_setData,
	.MemSet = MEMNODE_MemSet,
	.MemCopy = MEMNODE_MemCopy,
	.MemCopy2 = MEMNODE_MemCopy2,
	.MemConcat = MEMNODE_MemConcat,
	.MemConcat2 = MEMNODE_MemConcat2,
	.Print = MEMNODE_Print,
};

MemoryNode* MEMNODE_Create()
 {	
	MemoryNode *new_node;
	new_node = (MemoryNode*) malloc(sizeof(MemoryNode));

	MEMNODE_Init(new_node);

	return new_node;
}

s16 MEMNODE_Init(MemoryNode *node_)
 {
	//No need to check errors only use in Create

	node_->ops_ = &memory_node_ops;
	node_->data_ = NULL;
	node_->size_ = 0;
	node_->next_ = NULL;
	node_->prev_ = NULL;
	return kErrorCode_Memory;	
}

s16 MEMNODE_CreateFromRef(MemoryNode **node)
 {
	*node = MEMNODE_Create(*node);
	
	return kErrorCode_Ok;
}

void* MEMNODE_Data(MemoryNode *node) // returns a reference to data_
 {
	return node->data_;
}

u16	MEMNODE_Size(MemoryNode *node) // returns data size
 {
	return node->size_;
}

static s16 MEMNODE_setData(MemoryNode *node, void *src, u16 bytes){

	if(NULL == node){
		return 	kErrorCode_Node_NULL;
	}
	node->size_ = bytes;
	node->data_ = malloc(sizeof(bytes));
	node->ops_->MemSet(node,0);
	*((u8*)node->data_) = *(u8*)src;
	return kErrorCode_Ok;
	
}

static s16 MEMNODE_MemSet(MemoryNode *node, u8 value){

	if(NULL == node){
		return kErrorCode_Node_NULL;
	}
	if(NULL== node->data_){	
		return kErrorCode_NULL;
	}
	memset(node->data_,value, node->size_);
	
	return kErrorCode_Ok;
}

static void MEMNODE_Print(MemoryNode *node){
	
	printf("size: [%d]\n ",node->size_);
	if( node != NULL){
	//Create a count to not move the pointer  exactly  the size
	u16 count = 0;;
	//Use aux to move trough
	u8* data_aux = node->data_;
	while(count < node->size_){
		//Show Hex----- decim----- char
		printf("%04X -------%04d -------%c\n",*data_aux,*data_aux,*data_aux);
		data_aux++;
		count++;
	 }
	}else{
		printf("Error on node trying to print a NULL \n");
	}

}

static s16 MEMNODE_MemCopy(MemoryNode *node, void *src, u16 bytes){

	if(NULL == src){
		return	kErrorCode_NULL;
	}
	if(NULL == node){
		return kErrorCode_Node_NULL;
	}
	//The node->data_ we not longer need it and dont need to save it 
	//so we just free it
	free(node->data_);
	//Save now the new size_ we going to need for the value copy
	node->data_ = malloc(sizeof(bytes));
	memset(node->data_,0,bytes);
	u16 count = 0;
	//Use aux to move trough
	u8* data_aux = node->data_;
	u8* aux_node  = src;
	while(count < node->size_){
		*data_aux++ = *aux_node++;
		count++;
	 }
	node->size_ = bytes;

	data_aux = NULL;
	aux_node = NULL;

	return kErrorCode_Ok;
	
}

static s16 MEMNODE_MemCopy2(MemoryNode *dest,MemoryNode *src){
	//Check if source and destination are not NULL
	if(NULL == src){
		return kErrorCode_Node_NULL;
	}
	if (NULL == dest){
		return kErrorCode_Node_NULL;
	}
	//Check if the data of the source is not poiting NULL
	if(NULL == src->data_){
		return kErrorCode_NULL;
	}
	//Check in case that destination data is poiting something 
	if(NULL != dest->data_){ 
	free(dest->data_);
	}

	dest->data_ = malloc(MEMNODE_Size(src));

	if(dest->data_ == NULL){
		return kErrorCode_Memory;
	}

	memcpy(dest->data_,src->data_,src->size_);
	dest->size_ = src->size_;

	return kErrorCode_Ok;
	
}

static s16 MEMNODE_MemConcat(MemoryNode *node, void *src, u16 bytes){

  if(NULL == node){
	return kErrorCode_Node_NULL;
 }

 if(NULL == src){
	return kErrorCode_NULL;
 }
 if(NULL == node->data_){
	 return kErrorCode_NULL;
 }
	//Create an aux node for dont lose the data we were poiting
	u8  *aux_node;
	aux_node =  malloc(node->size_+ bytes);

	//In case we reserve memory on some point that has trash on we clean it
	memset(aux_node,0,node->size_+ bytes);

	//Point to start of the memory for dont lose the start as we going to move using aux_node
	u8* data_node = (u8*) aux_node;
 
	//Create a count var for while we are moving trough the memory move the same as the size 
	u16 count = 0;

	// Create a data aux so we can move trough the memory of the node data
	u8* data_aux = node->data_;

	//Now copy the first bit  to our aux node
	*aux_node = *data_aux;
	if(node->size_ > 1 ){

		// Go trough all the node data
		while(count < node->size_){
			*aux_node++=*data_aux++;
			count++;
		}
	}else{
		*aux_node++;
	}
	// We reset for dont start at the same point we end 
	count = 0;

	//Point now to src
	data_aux = src;

	//Insert again the first memory bit
	*aux_node = *data_aux;

	if(bytes > 1 ){
	 // Go trough all the node src
		while(count < bytes){
			*aux_node++=*data_aux++;
			count++;
		}
	}

	//Now free the data where node where pointing at
	free(node->data_);
		
	// Now node point the concatenated memory we created
	node->data_ = data_node;
   
	// Increase the size_ of the concatenated node
	node->size_ +=bytes; 

	//All the pointers now point to NULL
	aux_node = NULL;
	data_node= NULL;
	data_aux = NULL;

  return kErrorCode_Ok;
}

static s16 MEMNODE_MemConcat2(MemoryNode *node, MemoryNode *dest){

	MEMNODE_MemConcat(dest,node->data_,node->size_);

return kErrorCode_Ok;
}

