// prj_list.c : Defines the entry point for the console application.
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//


#include "../include/adt_dlist.h"
#include "memory_node.c"

DList* DLIST_Create(){
	DList *new_list;
	new_list =  malloc(sizeof(DList));
	new_list->head_ = NULL;
	new_list->tail_ = NULL;
	new_list->total_elem_= 0;

	return new_list;
}
 s16 DLIST_Init(DList *dlist){

	MemoryNode *node_ = MEMNODE_Create();
	dlist->head_ = node_;
	dlist->tail_ = node_;
	dlist->total_elem_ = 0;

	return kErrorCode_Ok;
}

bool isEmpty(DList *dlist){

	if(0 == dlist->total_elem_){
		return true;
	}
	return false;
}
u16 Length(DList *dlist){
	return dlist->total_elem_;
}
s16 InsertFirst(DList *dlist,void *value,u16 bytes){
	Insert(dlist,value,0,bytes);
	  return kErrorCode_Ok;
}
s16 InsertLast(DList *dlist,void *value,u16 bytes){
	Insert(dlist,value,dlist->total_elem_,bytes);
	  return kErrorCode_Ok;
}
s16 Insert(DList *dlist, void *value, u16 position_,u16 bytes){

	MemoryNode *aux_prev = MEMNODE_Create();

	MemoryNode *node_data_ = MEMNODE_Create();
	node_data_->ops_->setData(node_data_,value,bytes);
	
	if( position_ > 0){

		aux_prev = dlist->head_;

		u8 count  = 0;

		while(count < position_-1){
		aux_prev = aux_prev->next_;
			count++;
		}

		node_data_->next_ =  aux_prev->next_;
        aux_prev->next_->prev_ = node_data_;

		aux_prev->next_ =  node_data_;
        node_data_->prev_ = aux_prev;

		if(position_ == dlist->total_elem_){
			dlist->tail_ = node_data_;
		}

	}else{
	
		node_data_->next_ = dlist->head_;
        dlist->head_->prev_ = node_data_;
		dlist->head_ = node_data_;
	}
	

	dlist->total_elem_++;


 return kErrorCode_Ok;
}
// Head: returns a reference to the first node
MemoryNode **Head(DList *dlist){
		return &dlist->head_;
}

MemoryNode *ExtractFirst(DList *dlist){
	return Extract(dlist,0);
}
MemoryNode *ExtractLast(DList *dlist){
	return Extract(dlist,dlist->total_elem_);
}
MemoryNode *Extract(DList *dlist, u16 position_){

	MemoryNode *aux;
	MemoryNode *extract;
	aux = dlist->head_;
	if(position_ > 0){


	u8 count  = 0;

	while(count < position_-1){
		aux = aux->next_;
			count++;
	}


	extract = aux->next_;
	aux->next_ = aux->next_->next_;
    aux->next_->prev_ = aux;
	}else{
		extract = dlist->head_;
		dlist->head_ = dlist->head_->next_;
        dlist->head_->prev_ = NULL;
	}
	dlist->total_elem_--;
	return extract;
}
u16 Concat(DList* src,DList* dest){

	dest->tail_->next_=src->head_;
    src->head_->prev_ = dest->tail_;

	dest->tail_ = src->tail_;
	dest->total_elem_ += src->total_elem_;
	return kErrorCode_Ok;
}

u16 List_Traverse(DList *dlist, void(*callback) (MemoryNode *)){

	MemoryNode* aux = dlist->head_;
  for(int i = 0; i < dlist->total_elem_ ; i++){

     callback(aux);
	 aux = aux->next_;

 }

return kErrorCode_Ok;
}


s16 Print(DList *dlist){

	MemoryNode *aux;
	aux = dlist->head_;
	for(int i = 0; i< dlist->total_elem_; i++){
		aux->ops_->Print(aux);
		aux = aux->next_;
	}

	return kErrorCode_Ok;
}
