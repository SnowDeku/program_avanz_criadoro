/*
Programacion Avanzada 
Jorge Criado Ros

*/


#include "../include/character.h"

void setLife(unsigned int life){
    life_ = life;
}
void setAtack(unsigned int atack){
    atack_ = atack;
}
void setDefense(unsigned int defense){
    defense_ = defense;
}

void setMovement(unsigned int movement){
    movement_ = movement;
}
void setSp_atack(unsigned int sp_atack){
    sp_atack_ = sp_atack;
}
void setExperience(unsigned int experience){
    experience_ = experience;
}
void setInitPosition(unsigned int pos_x,unsigned int pos_y){
    pos_x_ = pos_x;
    pos_y_ = pos_y;
}
void Enabled(bool enabled){
    enabled_  = enabled;
}