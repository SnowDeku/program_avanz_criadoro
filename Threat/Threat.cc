#include <thread>
#include <iostream>
#include <vector>


void hello(){

std::cout << "HELLOW MELOW\n" << std::endl;
//printf("\n hello world ");

return;
}


int main(){
    int num_Cores;
    //Miramos cual es el total de cores del ordenador
    num_Cores = std::thread::hardware_concurrency();

    printf("Ver Hardware Cores : %d\n",num_Cores);
    //Creamos vector de hilos donde guardaremos las funciones de los hilos
    std::vector<std::thread> vec_thread;


    for(int i = 0 ;i < num_Cores-1 ; i++){
        vec_thread.push_back(std::thread(hello));
    }

    hello();

    for(int i = 0; i < vec_thread.size();i++){
        vec_thread[i].join();   
    }
    std::cin.get();

    return 0;
}